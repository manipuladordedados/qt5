local qt = require 'qt5'

local qml = qt.load_qml(byte_span.append([[
    import QtQml 2.0

    QtObject {
        function foobar(code: int) {
            Qt.exit(code)
        }
    }
]]))

qml.engine['exit(int)'] = function(x)
    print(x)

    -- clear cycles/references so this VM can die and application exit
    spawn(function() qml.engine['exit(int)'] = nil end):detach()
end

qml.object('foobar(int)', 42)
