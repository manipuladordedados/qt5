= qt5.qml_context.property

:_:

ifeval::["{doctype}" == "manpage"]

== Name

qt5.qml_context.property - a handle to the properties of a QQmlContext instance

== Description

endif::[]

This class represents a handle to the properties of an instance of QQmlContext.

== Metamethods

=== `__index()`

Read property specified by key.
