/* Copyright (c) 2023 Vinícius dos Santos Oliveira

   Distributed under the Boost Software License, Version 1.0. (See accompanying
   file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt) */

#pragma once

#include <thread>

#include <emilua/core.hpp>

namespace emilua_qt {

struct qt_handle
{
    qt_handle();
    ~qt_handle();

    std::error_code (*const init_lua_module)(lua_State* L);

private:
    std::thread thread;
    void (*const loop_end)();
};

} // namespace emilua_qt
