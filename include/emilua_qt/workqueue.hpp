/* Copyright (c) 2023 Vinícius dos Santos Oliveira

   Distributed under the Boost Software License, Version 1.0. (See accompanying
   file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt) */

#pragma once

#include <QtCore/QObject>
#include <QtCore/QEvent>

namespace emilua_qt {

class WorkUnitEvent : public QEvent
{
public:
    WorkUnitEvent(std::function<void()> closure);

    std::function<void()> closure;
    static int event_type;
};

class WorkQueue : public QObject
{
    Q_OBJECT
public:
    explicit WorkQueue(QObject* parent = nullptr);

    bool event(QEvent* e) override;
};

} // namespace emilua_qt
