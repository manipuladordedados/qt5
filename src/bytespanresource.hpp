/* Copyright (c) 2023 Vinícius dos Santos Oliveira

   Distributed under the Boost Software License, Version 1.0. (See accompanying
   file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt) */

#include <emilua_qt/bytespanresource.hpp>

namespace emilua_qt {

ByteSpanResource::ByteSpanResource(QObject* parent)
    : QObject{parent}
{}

} // namespace emilua_qt
