/* Copyright (c) 2023 Vinícius dos Santos Oliveira

   Distributed under the Boost Software License, Version 1.0. (See accompanying
   file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt) */

#include <emilua_qt/workqueue.hpp>

namespace emilua_qt {

int WorkUnitEvent::event_type = QEvent::registerEventType();

WorkUnitEvent::WorkUnitEvent(std::function<void()> closure)
    : QEvent{static_cast<QEvent::Type>(event_type)}
    , closure{std::move(closure)}
{}

WorkQueue::WorkQueue(QObject* parent)
    : QObject{parent}
{}

bool WorkQueue::event(QEvent* e)
{
    if (e->type() == WorkUnitEvent::event_type) {
        auto work = static_cast<WorkUnitEvent*>(e);
        work->closure();
        return true;
    }
    return QObject::event(e);
}

} // namespace emilua_qt
